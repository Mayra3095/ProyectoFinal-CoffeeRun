
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/30/2015 20:12:06
-- Generated from EDMX file: C:\Users\DMPD\documents\visual studio 2013\Projects\Proyecto_CoffeeRun\Proyecto_CoffeeRun\Models\ModelCoffeeRun.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [dbCoffeeRun];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[paginaSet1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[paginaSet1];
GO
IF OBJECT_ID(N'[dbo].[productoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[productoSet];
GO
IF OBJECT_ID(N'[dbo].[usuarioSet1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuarioSet1];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'productoSet'
CREATE TABLE [dbo].[productoSet] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [descripcion] nvarchar(max)  NULL,
    [precio] int  NOT NULL,
    [tipo] nvarchar(max)  NOT NULL,
    [imagen] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'usuarioSet1'
CREATE TABLE [dbo].[usuarioSet1] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [contrasena] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'paginaSet1'
CREATE TABLE [dbo].[paginaSet1] (
    [id] int IDENTITY(1,1) NOT NULL,
    [titulo] nvarchar(max)  NOT NULL,
    [contenido] nvarchar(max)  NOT NULL,
    [menu] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'categoriaSet'
CREATE TABLE [dbo].[categoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'productocategoria'
CREATE TABLE [dbo].[productocategoria] (
    [producto_id] int  NOT NULL,
    [categoria_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'productoSet'
ALTER TABLE [dbo].[productoSet]
ADD CONSTRAINT [PK_productoSet]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'usuarioSet1'
ALTER TABLE [dbo].[usuarioSet1]
ADD CONSTRAINT [PK_usuarioSet1]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'paginaSet1'
ALTER TABLE [dbo].[paginaSet1]
ADD CONSTRAINT [PK_paginaSet1]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [Id] in table 'categoriaSet'
ALTER TABLE [dbo].[categoriaSet]
ADD CONSTRAINT [PK_categoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [producto_id], [categoria_Id] in table 'productocategoria'
ALTER TABLE [dbo].[productocategoria]
ADD CONSTRAINT [PK_productocategoria]
    PRIMARY KEY CLUSTERED ([producto_id], [categoria_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [producto_id] in table 'productocategoria'
ALTER TABLE [dbo].[productocategoria]
ADD CONSTRAINT [FK_productocategoria_producto]
    FOREIGN KEY ([producto_id])
    REFERENCES [dbo].[productoSet]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [categoria_Id] in table 'productocategoria'
ALTER TABLE [dbo].[productocategoria]
ADD CONSTRAINT [FK_productocategoria_categoria]
    FOREIGN KEY ([categoria_Id])
    REFERENCES [dbo].[categoriaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_productocategoria_categoria'
CREATE INDEX [IX_FK_productocategoria_categoria]
ON [dbo].[productocategoria]
    ([categoria_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------