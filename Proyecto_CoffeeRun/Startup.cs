﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Proyecto_CoffeeRun.Startup))]
namespace Proyecto_CoffeeRun
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
