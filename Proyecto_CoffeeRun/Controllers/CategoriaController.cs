﻿using PagedList;
using Proyecto_CoffeeRun.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_CoffeeRun.Controllers
{
    public class CategoriaController : Controller
    {
        public ModelCoffeeRunContainer db = new ModelCoffeeRunContainer();

        // GET: Productos
        public ActionResult Index(int page = 1, int pageSize = 9)
        {
            List<categoria> categorias = db.categoriaSet.ToList();
            PagedList<categoria> pageCategorias = new PagedList<categoria>(categorias, page, pageSize);

            return View(pageCategorias);
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear([Bind(Include = "Id,nombre")] categoria categoria)
        {
            if (ModelState.IsValid)
            {
                db.categoriaSet.Add(categoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoria);
        }

        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            categoria categoria = db.categoriaSet.Find(id);

            if (categoria == null)
            {
                return HttpNotFound();
            }

            return View(categoria);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "Id,nombre")] categoria categoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoria);
        }

        public ActionResult Eliminar(int id)
        {
            categoria categoria = db.categoriaSet.Find(id);
            db.categoriaSet.Remove(categoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}