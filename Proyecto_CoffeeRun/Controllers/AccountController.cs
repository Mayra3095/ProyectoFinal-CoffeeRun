﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_CoffeeRun.Models;
using System.Web.Security;



namespace Proyecto_CoffeeRun.Controllers
{
    public class AccountController : Controller
    {
        ModelCoffeeRunContainer db = new ModelCoffeeRunContainer();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(usuario usuario)
        {
            if (ModelState.IsValid)
            {
                usuario dbUsuario = db.usuarioSet1.SingleOrDefault(u => u.nombre == usuario.nombre && u.contrasena == usuario.contrasena);

                if (dbUsuario != null)
                {
                    FormsAuthentication.SetAuthCookie(usuario.nombre, false);
                    Session["userName"] = usuario.nombre;

                    return RedirectToAction("Index", "Home", new { nombre = usuario.nombre });
                }
                else
                {
                    ModelState.AddModelError("", "Usuario y/o contreña incorrecto(s)");
                }
            }


            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}